contextplot - context based plotting for matplotlib
===================================================


Contextplot contains powerful facilities for cleaner plotting code abstracting
repetitive code patterns when using matplotlib in a OO manner.

Planned features:

- context creating figure similar to standard python filehandles
- automatically skip contextblocks if plot already exists
- expose either complete figure or pass a single axes for drawing
