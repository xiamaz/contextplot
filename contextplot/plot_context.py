import contextlib
from matplotlib.backends import backend_agg
from matplotlib import figure as mfigure


def plot_to_file(figure, *args, **kwargs):
    """Save created figure to a file."""
    backend_agg.FigureCanvasAgg(figure)
    figure.savefig(*args, **kwargs)


class Figure(contextlib.ContextDecorator):
    """
    Create return a created figure.
    """
    def __init__(
            self, path, *args, **kwargs
    ):
        # preload the exitargs into the exit function
        self._figure = None
        self._figure_args = args
        self._figure_kwargs = kwargs

        self._path = path

    def __enter__(self):
        """Create a new figure with given arguments."""
        self._figure = mfigure.Figure(
            *self._figure_args,
            **self._figure_kwargs
        )
        return self._figure

    def __exit__(self, *exc):
        """Save the figure to the saved path."""
        backend_agg.FigureCanvasAgg(self._figure)
        self._figure.savefig(self._path)
        return False


class SinglePlot(Figure):
    """Create a figure with a single axes which will be saved to a file."""
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._axes = None

    def __enter__(self):
        """Return the single axes from the plot."""
        figure = super().__enter__()
        self._axes = figure.add_subplot(111)
        return self._axes
